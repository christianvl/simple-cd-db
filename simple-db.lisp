;;;; Simple file to learn basics of common lisp
;;;; Chapter 3 from https://gigamonkeys.com/book/practical-a-simple-database.html

;;; simple list
(list 1 2 3)

;;; properties list = plist
;;; the colon : denotes a keyword symbol (name)
;;; the ** is a naming convention for global variables
(defvar *test* (list :a 1 :b 2 :c 3))

;;; GETF finds a value in a list based on a symbol
(getf *test* :c)
(getf (list :a 1 :b 2 :c 3) :c)

;;; function that will take four fields as arguments and return plist
(defun make-cd (title artist rating ripped)
  (list :title title :artist artist :rating rating :ripped ripped))

;;; and the function call
(make-cd "Iron Maiden" "Running Free" 8 t)

;;; create a variable to store the dataset of records
(defvar *db* nil)

;;; Create a new function to use PUSH to add items to the db
(defun add-record (cd) (push cd *db*))

;;; Function call:
(add-record (make-cd "Running Free" "Iron Maiden" 8 t))
(add-record (make-cd "Braking the Law" "Judas Priest" 7 t))
(add-record (make-cd "Back in Black" "ACDC" 9 nil))

;;; To see the contents
*db*
;;; With better formatting
(defun dump-db ()
  (dolist (cd *db*)
    (format t "~{~a:~10t~a~%~}~%" cd)))
;;; ~ = next char is a command
;;; a = argument (variable that will be printed)
;;; { = next argument is in a list that will be looped } = end of the list
;;; 10t = chars to tabulate
;;; % = new line

(defun dump-db2 ()
  (format t "~{~{~a:~10t~a~%~}~%~}" *db*))

;;; general function to get user input
(defun prompt-read (prompt)
  (format *query-io* "~a: " prompt) ; emit a prompt
  (force-output *query-io*)         ; ensure lisp will not wait for new line before printing prompt
  (read-line *query-io*))           ; read on line from the terminal connection *query-io*

;;; get input for each cd
(defun prompt-for-cd ()
  (make-cd
   (prompt-read "Title")
   (prompt-read "Artist")
   ;; parse input to integer, discard chars, or set to 0 if NAN
   (or (parse-integer (prompt-read "Rating") :junk-allowed t) 0)
   ;; built-in function for y/n input
   (y-or-n-p "Ripped [y/n]: ")))

;;; function to loop input for new cds
(defun add-cds ()
  (loop (add-record (prompt-for-cd))
        (if (not (y-or-n-p "Another? [y/n]: ")) (return))))

;;; Save/Load db to/from file
(defun save-db (filename)
  (with-open-file (out filename ; open file and binds stream to the out variable
                       ; parameters
                       :direction :output ; open the file for writing
                       :if-exists :supersede) ; overwrite
    (with-standard-io-syntax ; ensure certain variables are set to standard values for print
      (print *db* out))))

;;; save to a file
(save-db "test.db")

;;; Load DB function
(defun load-db (filename)
  (with-open-file (in filename) ; parameters not needed - default is read
    (with-standard-io-syntax
    (setf *db* (read in))))) ; setf set val1 with output from val2

;;; load DB from a file
(load-db "test.db")

;;; querying the DB
;;; using remove-if-not = subset a list with the given criteria
(remove-if-not #'evenp '(1 2 3 4 5 6 7 8 9 10)) ;evenp returns even numbers from a list
;;; using an anonymous function
(remove-if-not #'(lambda (x) (= 0 (mod x 2))) '(1 2 3 4 5 6 7 8 9 10))
(remove-if-not #'(lambda (x) (= 1 (mod x 2))) '(1 2 3 4 5 6 7 8 9 10))

;;; using remove-if-not with an artist
(remove-if-not #'(lambda (cd) (equal (getf cd :artist) "Iron Maiden")) *db*)
;;; using a function by list item to search
(defun select-by-artist (artist)
  (remove-if-not #'(lambda (cd)
                     (equal (getf cd :artist) artist))
                 *db*))
;;; however, it's possible to write one generic function that takes a function as an argument
(defun select (selector-fn)
  (remove-if-not selector-fn *db*))
;;; and the call would be
(select #'(lambda (cd) (equal (getf cd :artist) "Iron Maiden")))
;;; also, the call can be wrapped in a function
(defun artist-selector (artist)
  #'(lambda (cd) (equal (getf cd :artist) artist)))
(select (artist-selector "Iron Maiden"))

;;; The problem is that this still requires a by type function creation
;;; On the other hand, it is possible to create a function with keyword parameters
(defun foo (&key a (b 20) (c 30 c-p)) list (a b c c-p))
;;; &key means the function call is parameterized with keyword + value
(foo :a 1 :b 2 :c 3)
;;; But the function can also be called with no arguments and missing values are set to nil
;;; the argument (b 20) means that 20 will be the default value if none is provided
;;; and (c 30 c-p) c-p is supplied-p = t if a value was provided or nil if using default

;;; applying the concept to the DB
(defun where (&key title artist rating (ripped nil ripped-p))
  #'(lambda (cd)
      ;; use all parameters set in the search
      (and
       (if title (equal (getf cd :title) title) t)
       (if artist (equal (getf cd :artist) artist) t)
       (if rating (equal (getf cd :rating) rating) t)
       ;; check if user specified ripped (keep in mind: unset values are NIL)
       (if ripped-p (equal (getf cd :ripped) ripped) t))))
(select (where :rating 9 :ripped nil))

;;; Updating DB records
;;; takes a function and keyword: value as arguments
(defun update (selector-fn &key title artist rating (ripped nil ripped-p))
  (setf *db* ; setf defines a variable value
        (mapcar ; maps over a list (*db* in this case) = apply function to each element in sequence MAPCAR function sequence
         #'(lambda (row) ; anonymous function takes a row executes code and returns a row
             (when (funcall selector-fn row) ; when (t) = call function
               (if title (setf (getf row :title) title)) ; get item in row and set with new item
               (if artist (setf (getf row :artist) artist))
               (if rating (setf (getf row :rating) rating))
               (if ripped-p (setf (getf row :ripped) ripped)))
             row) *db*)))
(update (where :artist "Iron Maiden") :rating 10) ; updates all Iron Maiden to rating 10

;;; Delete a record from the DB
(defun delete-rows (selector-fn)
  (setf *db* (remove-if selector-fn *db*))) ; remove if returns a list with the item removed
(delete-rows (where :title "Braking the Law"))


;;; Removing code duplication

;;; Replacing the (where) with a macro
;;; A macro passes the arguments unevaluated, then its return value is evaluated.
(defmacro backwards (expr) (reverse expr)) ; reverse returns the reverse of a list
(backwards ("hello, world" t format)) ;  in a function the values would be illegal
; but in the macro, the return value will be (format t "hello, world") that will be evaluated

;;; In addition, the ' symbol, prevents an expression from being evaluated
(defun make-comparison-expr (field value)
  (list 'equal (list 'getf 'cd field) value))
(make-comparison-expr :rating 10)

;;; The ` symbol also prevents the evaluation of an expression, but anything following a , will
`(1 2 (+ 1 2)) ; = (1 2 (+ 1 2))
`(1 2 ,(+ 1 2)) ; = (1 2 3)

(defun make-comparison-expr (field value)
  `(equal (getf cd ,field) ,value))

(defun make-comparisons-list (fields)
  (loop while fields
        collecting (make-comparison-expr (pop fields) (pop fields))))

(defmacro where (&rest clauses)
  `#'(lambda (cd) (and ,@(make-comparisons-list clauses))))

;;; @ is a splice symbol, breaks the list values to parent parentheses
;;; `(and ,(list 1 2 3))   ==> (AND (1 2 3))
;;; `(and ,@(list 1 2 3))  ==> (AND 1 2 3)
;;; `(and ,@(list 1 2 3) 4) ==> (AND 1 2 3 4)

;;; &resr != &keys. &rest == arbitrary number of arguments

;;; Can use macroexpand-1 to get the unevaluated return value of a macro
(macroexpand-1 '(where :title "Running Free" :ripped t))
(select (where :title "Running Free" :ripped t))
